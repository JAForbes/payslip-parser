#!/usr/bin/env node
const fs = require('fs');
const path = require('path')
const pdf = require('pdf-parse');
const R = require('ramda')

const paths = process.argv.slice(2)

if( paths.length == 0 ){
    console.error('Please specify a space delimited list of files to process.')
    console.error(
        'npx payslip-parser /my-payslips/**.pdf'
    )
    process.exit(1)
}
 
const run = () => 
    paths.map(
        filepath => ({
            filename: path.basename(filepath),
            buffer: fs.readFileSync(filepath),
            filepath
        })
    )
    .reduce(
        async (p, { filename, buffer }) => {
            const items = await p
            const drivers = [XERO, XERO2, HERO]

            const response = await pdf(buffer, {
                pagerender: 
                    pageData => pageData.getTextContent() 
                        .then( content => content.items.map( x => x.str ))
                        .then(
                            items => 
                                [drivers.find( x => x.match(items) == x.signature )]
                                .filter(Boolean)
                                .map(
                                    driver =>
                                    ({ 
                                        filename
                                        , parsed: driver.parse(items)
                                        , driverName: driver.name 
                                    })
                                )
                                .concat([
                                    ({ 
                                        filename
                                        , parsed: items
                                        , driverName: 'RAW' 
                                    })
                                ])
                                .shift()
                        )
                        .then(JSON.stringify)
            })

            return items.concat(JSON.parse(response.text))
        }
        ,[]
    )

const HERO = {
    signature: '0|1|2',
    name: 'Employment Hero',
    match(items){
        const i = s => items.findIndex( x => x.includes(s) )
        
        const summary = i('SUMMARY')
        const signature = [
            i('SUMMARY')
            , i('EARNINGS')
            , i('SUPER CONTRIBUTIONS')
        ]
        .map(
            x => x - summary
        )
        .join('|')

        return signature
    },
    parse(items){
        const i = s => items.findIndex( x => x.includes(s) )
        const payee = (() => {
            const [h1, name, street, suburb, otherAddress] = 
                items.slice(i('Pay advice for'), i('Pay period:'))

            return {
                name,
                address: {
                    street,
                    suburb: suburb + ' ' + otherAddress
                }
            }
        })()

        
        const variables = (() => {
            const [theirPeriod, theirDate, theirPaid, theirRate, theirEmploymentType] = 
                items.slice(
                    i('Pay period:'),
                    i('This Pay')
                )
            
            const [period, paid, date, rate, employmentType] = 
                [theirPeriod, theirPaid, theirDate, theirRate, theirEmploymentType].map( x => x.split(': ')[1] )
            
            return {
                period, paid, date, rate, employmentType
            }
        })()

        const summary = (() => {
            const [
                h1
                , h2
                , gross
                , h3
                , payg
                , h4
                , superannuation
                , h5
                , net
            ] =  items.slice(
                i('This Pay'),
                i('Details')
            )
            
            return {
                gross
                , payg
                , superannuation
                , net
            }
        })()

        const earnings = (() => {
            const raw = items.slice(i('Details'), i('Type')).slice(1)
            const hours = raw[4]
            const rate = raw[5]
            const gross = raw[6]
            
            return {
                hours, rate, gross
            }
        })()

        const superannuation = (() => {
            const [fund, expectedPaymentDate, amount] = 
                items.slice(i('Type')).slice(3,6)

            return {
                fund, expectedPaymentDate, amount
            }
        })()

        const leave = (() => {
            const [accrued] = items.slice(i('Accrued')).slice(3)

            return { accrued }
        })()
    
        return { 
            payee
            , variables
            , summary
            , earnings
            , superannuation 
            , leave
        }
    }
}

const XERO = {
    signature: '3|8|23',
    name: 'XERO',
    match(items){
        const i = s => items.findIndex( x => x == s)
        
        return [
            'PAID BY'
            , 'EMPLOYMENT DETAILS'
            , 'SALARY & WAGES'
        ]
        .map(i)
        .join('|')
    },
    parse(items) {
        let rest = items
        
        const i = s => rest.findIndex( x => x == s)
    
        const payee = (() => {
            const [name, street, suburb] = rest.slice(0, i('PAID BY'))
            return { name, address: { street, suburb } }
        })()
        
        const payer = (() => {
            const [h1, company, street, suburb, ABN] = rest.slice(
                i('PAID BY'), i('EMPLOYMENT DETAILS')
            )
    
            return {
                company, address: { street, suburb }, ABN
            }
        })()
    
        const variables = (() => {
            const [h1, theirFrequency, theirSalary, theirBasis, junk] = 
                rest.slice(
                    i('EMPLOYMENT DETAILS'),
                    i('Pay Period: ')
                )
            
            const [frequency, salary, basis] = 
                [theirFrequency, theirSalary, theirBasis].map( x => x.split(': ')[1] )
            
            return {
                frequency, salary, basis
            }
        })()
    
        const summary = (() => {
            const [h1, theirPeriod, h2, paymentDate, h3, gross, h4, net] = 
                rest.slice(
                    i('Pay Period: '),
                    i('THIS PAY')
                )
    
            const period = theirPeriod.split(' - ')
    
            return {
                period, paymentDate, gross, net
            }
        })()
    
        const row = (startWord, endWord) => {
            const raw = rest.slice(
                i(startWord),
                i(endWord)
            )
    
            const rawItems = raw.slice(1, -3)
    
            const items = R.splitEvery(3, rawItems).map(
                ([name, thisPay, ytd]) => ({ name, thisPay, ytd})
            )
    
            const [totalPay, totalYTD] = raw.slice(-2)
    
            return {
                items, totalThisPay: totalPay, totalYTD
            }
        }
    
        const salary = (() => {
            const [h1, h2, h3, hours, rate, thisPay, ytd, h4, totalPay, totalYTD] = rest.slice(
                i('SALARY & WAGES'),
                i('TAX')
            )
    
            return {
                hours, rate, thisPay, ytd, totalPay, totalYTD
            }
        })()
    
        const superannuation = row('SUPERANNUATION', 'LEAVE') 
        const tax = row('TAX', 'SUPERANNUATION')
        
        const leave = (() => {
            const [h1,h2,h3,h4,h5, totalPay, used, ytd] = rest.slice(
                i('LEAVE'),
                i('PAYMENT DETAILS')
            )
    
            return {
                totalPay, used, ytd
            }

        })()
    
        const paymentDetails = (() => {
            const [h1, h2, h3, accountNumber, accountName, reference, amount] = 
                rest.slice(
                    i('PAYMENT DETAILS')
                )
    
            return { accountNumber, accountName, reference, amount}
        })
    
        return {
            payee
            , payer
            , variables
            , summary
            , salary
            , tax
            , superannuation
            , leave
            , paymentDetails
        }
    }
}

const XERO2 = {
    ...XERO,
    signature: '3|9|24',
    name: 'XERO2'
}

run()
    .then(JSON.stringify)
    .then(console.log)

    