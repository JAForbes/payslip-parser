payslip-parser
==============

A naive parser for a few payslip formats.

Supports

- XERO
- Employment Hero

If it can't recognize the format, `payslip-parser` simply runs the file through [pdf-parse](https://www.npmjs.com/package/pdf-parse) naively.

```bash
npx payslip-parser /documents/payslips/*.pdf
```

I recommend piping the results into `ramda-cli` or (`jq` if you prefer).

```bash
payslip-parser /documents/payslips/*.pdf | npx ramda-cli "pluck('driverName')"
[
    "Employment Hero",
    "XERO",
    "RAW"
]
```

#### Contributing

This parser is super naive, it might not handle your payslip if there's even a slight deviation in the structure.  PR's welcome!

[Repo](https://gitlab.com/JAForbes/payslip-parser)